import React, {Component} from 'react';
import {ListItem} from "@material-ui/core";
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Avatar from '@material-ui/core/Avatar';
import BookmarkIcon from '@material-ui/icons/Bookmark';

class ListviewItem extends Component {


    render() {
        return (

            //Bei  Click auf ListItem soll explizite Notiz angezeigt werden
            <ListItem onClick={() => {
                this.props.getNote(this.props.note)
            }}>
                {/*Definition des Aufbau eines ListView-Items*/}
                <ListItemAvatar>
                    <Avatar>
                        <BookmarkIcon/>
                    </Avatar>
                </ListItemAvatar>
                <ListItemText
                    primary={this.props.title}
                    secondary={this.props.text}
                />
                <ListItemSecondaryAction>
                    <IconButton aria-label="Delete" onClick={() => this.props.deleteNote(this.props.note)}>
                        <DeleteIcon/>
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        )

    };
}

export default ListviewItem;