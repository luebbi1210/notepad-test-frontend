import React, {Component} from 'react';
import {TextField} from "@material-ui/core";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import './Notepad.css';

//
class Notepad extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            note: {
                id: "",
                title: '',
                text: '',
            },

        }
    }

    //Start-Timer nach Bauen der Seite (mit Intervall) --> für Datumsanzeige
    componentDidMount() {
         this.timerID = setInterval(
             () => this.tick(),
             1000
         );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }


    //Funktion zur Anzeige der selektierten App
    componentWillReceiveProps(newProps) {
        this.setState({
            note: newProps.currentNote
        });
    }

    //Funktion für Timer -> Uhrzeitanzeige
    tick() {
        this.setState({
            date: new Date()
        });
    }

    //Change-Handler für Titel
    handleChangeTitle = (event) => {
        this.setState({
            note: {
                title: event.target.value,
                text: this.state.note.text,
            }
        });

    }

    //Change-Handler für Notiz
    handleChangeNote = (event) => {
        this.setState({
            note: {
                title: this.state.note.title,
                text: event.target.value,
            }
        })
    }


    //Funktion um ID zu checken -> falls ID vorhanden soll PUT-Funktion in Backend angesprochen, wenn nicht POST-Funktion
    checkID = () => {
        if(this.state.note.id > 0) {
            console.log("Edit:" + this.state.note.id);
            this.props.editNote(this.state.note)
        }
        else {
            console.log("Add:" + this.state.note);
            this.props.addNote(this.state.note)

        }
    }


    render() {
        // console.log(this.state.note.id);

        // console.log(this.state.note);

        //Datumsanzeige
        let datum = this.state.date.getDate() + '.' + (this.state.date.getMonth() + 1) + '.' + this.state.date.getFullYear();
        return (

            //Aufbau (HTML) Notepad
            <div className='notepad'>
                <div className='notepad-wrapper'>

                    <div className='App-sec-header'>
                        <div className='title'>
                            <div className='note-title'>
                                <h4 defaultValue="Lorem ipsum" id='titel'>{this.state.note.title}</h4>
                            </div>
                            <div className='date'>
                                <p id='datum'>{datum} {this.state.date.toLocaleTimeString()}</p>
                            </div>
                        </div>
                    </div>

                    {/*Bereich links zur Texteingabe*/}
                    <div className='App-textarea'>
                        <div className='note-left'>

                            {/*Texteingabe-Titel*/}
                            <div className='note-textfield-title'>
                                <TextField className='notepadTextfield'
                                           onChange={this.handleChangeTitle}
                                           placeholder={'Enter title...'}
                                           label='title'
                                           value={this.state.note.title}/>
                            </div>

                            {/*Texteingabe Notiz*/}
                            <div className='note-textfield'>
                                <TextField className='notepadTextfield'
                                           onChange={this.handleChangeNote}
                                           multiline={true}
                                           placeholder={'Hello, i am a note!'}
                                           label='note'
                                           value={this.state.note.text}/>
                            </div>

                            {/*Button zum Hinzufügen/Ändern*/}
                            <div className='btnAdd'>
                                <Fab onClick={() => { this.checkID() }}
                                     // this.state.note.id >>> 0 ? this.props.editNote(this.state.note) : this.props.addNote(this.state.note);
                                     aria-label="Add" className='addButton'>
                                    <AddIcon/>
                                </Fab>
                            </div>

                        </div>

                        {/*Bereich rechts zur Anzeige Notiz+Titel*/}
                        <div className='note-right'>
                            <div className='note-area'>
                                <div className='title-right'>
                                    <h3>{this.state.note.title}</h3>
                                </div>
                                <div className='paragraph'>
                                    <p>{this.state.note.text}</p>
                                </div>
                            </div>

                        </div>


                    </div>

                </div>

            </div>
        )
    }
    ;
}

export default Notepad;
