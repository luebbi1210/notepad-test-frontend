import React, {Component} from 'react';
import {List} from "@material-ui/core";
import ListviewItem from './ListviewItem';
import './Listview.css';

class Listview extends Component {

    //Nach Bauen der Seite, sollen gesamte Notizen angezeigt werden
    componentDidMount() {
        this.props.listAll();
    }

    render() {

        //Checken ob Notizen vorhanden sind, ansonsten ListView aufbauen
        let notes = null;
        if (this.props.notes != null){
            notes = (this.props.notes).map((element) =>
            <ListviewItem
                note = {element}
                key = {element.id}
                id = {element.id}
                title = {element.title}
                text = {element.text}
                deleteNote = {this.props.deleteNote}
                getNote = {this.props.getNote}
                listAll = {this.props.listAll}
            />
        )}

        //Aufruf Liste
        return(
            <List className="note-list">
                {notes}
            </List>
        )

    };
}

export default Listview;