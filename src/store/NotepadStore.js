import {ReduceStore} from 'flux/utils';
import NotepadDispatcher from '../dispatcher/NotepadDispatcher'
import { ActionTypes } from "../actions/Actions";

class NotepadStore extends ReduceStore {

    //Konstruktor
    constructor() {
        super(NotepadDispatcher)
    }

    getInitialState() {

        return {
            notes: null,
            currentNote: {
                id: "",
                title: "",
                text: ""
            },

        };
    }

    //Einbindung Store um Aktualität aller Notizen zu gewährleisten
    reduce(state, action) {
        switch (action.type) {
            case ActionTypes.LIST_ALL:
                state.notes = action.data;
                return {...state};
            case ActionTypes.GET_NOTE:
                state.currentNote = action.data;
                return {...state};
            default:
                return state;
        }
    }
}

export default new NotepadStore();
