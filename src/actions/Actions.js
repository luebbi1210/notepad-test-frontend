import NotepadDispatcher from '../dispatcher/NotepadDispatcher';
import axios from 'axios';

export const ActionTypes = {
    ADD_NOTE: 'ADD_NOTE',
    GET_NOTE: 'GET_NOTE',
    DELETE_NOTE: 'DELETE_NOTE',
    EDIT_NOTE: 'EDIT_NOTE',
    LIST_ALL: 'LIST_ALL',
}

//Action-Klasse mit Grundfunktionen zur Kommunikation mit DB über "axios"

// const ip = "http://172.20.10.6";
const ip = "http://localhost";
const port = "8080";
const adress = ip + ":" + port;

export const Actions = {

    //Funktion zum Hinzufügen einer Notiz -> Übergabe von Titel+Text, ID wird von JPA selber vergeben
    addNote(note) {

        axios.post(adress + "/api/note", {"title": note.title, "text": note.text}).then((response) => {
            NotepadDispatcher.dispatch({
                type: ActionTypes.ADD_NOTE,
                status: response.status
            });
        }).catch((error) => {
            console.log(error);
        }).then(() => {
                Actions.listAll();
            }
        )
    },

    //Funktion zum Holen einer Notiz -> Abfrage über ID
    getNote(note) {

        axios.get(adress + "/api/note", {params: {"id": note.id}}).then((response) => {
            NotepadDispatcher.dispatch({
                type: ActionTypes.GET_NOTE,
                data: response.data
            });
        }).catch((error) => {
            console.log(error);
        })
    },

    //Funktion zum Löschen einer Noitz -> Löschen über ID
    deleteNote(note) {

        axios.delete(adress + "/api/note", {params: {"id": note.id}}).then((response) => {
            NotepadDispatcher.dispatch({
                type: ActionTypes.DELETE_NOTE,
                status: response.status
            });
        }).catch((error) => {
            console.log(error);
        }).then(() => {
                Actions.listAll();
            }
        )


    },

    // Funktion zum Bearbeiten einer Notiz -> Übergabe über ID?
    editNote(note) {

        axios.put(adress + "/api/note", {"id": note.id, "title": note.title, "text": note.text}).then((response) => {
            NotepadDispatcher.dispatch({
                type: ActionTypes.EDIT_NOTE,
                status: response.status
            });
        }).catch((error) => {
            console.log(error);
        }).then(() => {
                Actions.listAll();
            }
        )
    },

    // editNote(note) {
    //
    //     axios.put(adress + "/api/note", {"title": note.title, "text": note.text}).then((response) => {
    //         NotepadDispatcher.dispatch({
    //             type: ActionTypes.EDIT_NOTE,
    //             status: response.status
    //         });
    //     }).catch((error) => {
    //         console.log(error);
    //     }).then(() => {
    //             Actions.listAll();
    //         }
    //     )
    // },

    //Funktion um Gesamtheit aller Notizen zu holen
    listAll() {
        axios.get(adress + "/api/note/all").then((response) => {
            NotepadDispatcher.dispatch({
                type: ActionTypes.LIST_ALL,
                data: response.data
            });
        }).catch((error) => {
            console.log(error);
        })
    },




}

