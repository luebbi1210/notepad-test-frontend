import React, {Component} from 'react';
import './App.css';
import Listview from './components/Listview';
import Notepad from "./components/Notepad";

//Hauptklasse in der HTML-Aufbau des Front-End's definiert ist
class App extends Component {
    render() {
        return (

            //Aufbau Website
            <div className="App-body">

                {/*Header*/}
                <div className="App-header">
                    <div className='notes-header'>
                        <h2 id='notes-heading'>Notes</h2>
                    </div>
                    <div className='title-header'>
                        <h2 id='notepad-heading'> Your Notepad</h2>
                    </div>
                </div>

                {/*Liste links*/}
                <div className="App-noteList">
                    <Listview
                        notes={this.props.notepadStore.notes}
                        listAll={this.props.listAll}
                        deleteNote={this.props.deleteNote}
                        getNote={this.props.getNote}
                    />
                </div>

                {/*Notepad-Main -> rechts*/}
                <div className="App-wrapper">
                    <Notepad
                        editNote={this.props.editNote}
                        addNote={this.props.addNote}
                        currentNote={this.props.notepadStore.currentNote}
                    />
                </div>
            </div>
        );
    }

}

export default App;
