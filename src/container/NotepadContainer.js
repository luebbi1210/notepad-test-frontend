import React, {Component} from 'react';
import {Container} from 'flux/utils';
import NotepadStore from '../store/NotepadStore';
import App from '../App';
import {Actions} from '../actions/Actions';

class NotepadContainer extends Component {

    static getStores() {
        return [
            NotepadStore,
        ];
    }

    //Definiton aller Methoden
    static calculateState() {
        return {
            notepadStore: NotepadStore.getState(),
            addNote: Actions.addNote,
            getNote: Actions.getNote,
            deleteNote: Actions.deleteNote,
            editNote: Actions.editNote,
            listAll: Actions.listAll,
        };
    }

    //Methode zum State aktualisieren
    render() {
        return (
            <App {...this.state} />
        );
    }
}

export default Container.create(NotepadContainer);
